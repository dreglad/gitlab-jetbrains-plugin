# Release process

## CI based release

We are still working on automating the release process through CI. Until that is done, please follow the section below.

## Manual Release

### Environment

You need to have the following software installed on your local machine:

- [OpenJDK 17](https://adoptium.net/temurin/releases/?version=17)
- Ruby 3.1

As an alternative you can build and run from a container image based on the provided Dockerfile in the repository.

To run the environment using Docker, follow the steps below:

```shell
export CONTAINER_IMAGE="eclipse-temurin-17-ruby-3-1"

# build the image
docker build --pull -t "${CONTAINER_IMAGE}:latest" .

# run the environment mounting the local folder
docker run -v $PWD:/gitlab --workdir="/gitlab" --rm -it "${CONTAINER_IMAGE}:latest" /bin/bash
```

To mount the environment using containerd instead, follow the steps below:

```shell
export CONTAINER_IMAGE="eclipse-temurin-17-ruby-3-1"

# build the image
nerdctl build -t "${CONTAINER_IMAGE}:latest" .

# run the environment mounting the local folder
nerdctl run -v $PWD:/gitlab --workdir="/gitlab" --rm -it "${CONTAINER_IMAGE}:latest" /bin/bash
```

### Preparation

Before releasing a new version you need to prepare a Merge Request with the following:

1. In `gradle.properties` increase the `plugin.version` number according to the [Semantic Version 2.0](https://semver.org/)
1. Run `:patchChangelog` task to move the changes in `CHANGELOG.md` from `[Unreleased]` section
   to the section describing the new version:

   ```shell
   ./gradlew patchChangelog
   ```

After the Merge Request is merged, tag the merged commit with the new version:

```shell
# Tag the release version with the same version as in the grade.properties file
git tag v0.1.1

# Push tags:
git push origin --tags

# Clean the environment and build a new plugin package:
./
./gradlew clean assemble

# Set required environment variables:
# token should have `api` permission
export GITLAB_API_PRIVATE_TOKEN=<YOUR-PAT-TOKEN-HERE>

# Run the release script:
cd scripts && bundle install
./ci-release.rb
```
