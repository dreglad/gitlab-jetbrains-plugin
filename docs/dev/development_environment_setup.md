# Development environment setup

Use these instructions to run the GitLab plugin locally.

Prerequisites:

- You have [created a GitLab account](https://gitlab.com/users/sign_up). It is free and it is awesome!
- You have [IntelliJ IDEA CE](https://www.jetbrains.com/idea/download)
  or [IntelliJ IDEA Ultimate](https://www.jetbrains.com/idea/download) installed.
- You have the [Gradle](https://www.jetbrains.com/help/idea/gradle.html) plugin for JetBrains installed
  and configured.

To do this:

1. [Fork and clone the project](#fork-and-clone-the-project).
1. [Run the plugin](#run-the-plugin).
1. [Run tests](#run-tests).
1. [Add documentation](#add-documentation).

## Fork and clone the project

1. Use your GitLab account to
   [fork this project](https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/forks/new).
   Need help? See this [guide to forking a project](https://docs.gitlab.com/ee/gitlab-basics/fork-project.html#doc-nav).
1. Go to your forked project. The URL should be similar to
   `https://gitlab.com/<username>/gitlab-jetbrains-plugin`.
1. On the right-hand side of the page, select **Clone**. Copy the SSH or HTTPS URL to clone the project to your local machine.
   Need help? See this [guide to cloning a project](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-a-repository).
1. In IntelliJ IDEA,
   [open your cloned project](https://www.jetbrains.com/help/idea/import-project-or-module-wizard.html#open-project).

## Run the plugin

To run the plugin in IntelliJ IDEA:

1. In IntelliJ IDEA, on the navigation bar, select **Run plugin**.
1. In the dropdown list, under **Run Configuration**, select **Run Plugin**.

## Run tests

To run tests in IntelliJ IDEA:

1. In IntelliJ IDEA, on the navigation bar, select **Run plugin**.
1. In the dropdown list, under **Run Configuration**, select **Run Tests**.

## Add documentation

If you add a new feature or change an existing feature, document it in the README.

To add documentation that includes a new image:

1. Add images into the `docs/assets` folder, and commit the changes.
1. Edit the README file, and insert full permalinks to your new images.
   The permalinks contain the commit SHA from your first commit, and are
   in the form of:

   ```plaintext
   https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/raw/<COMMIT_SHA>/docs/assets/imagename.png
   ```

1. Commit your text changes.
