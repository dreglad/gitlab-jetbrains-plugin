package com.gitlab.plugin

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.ui.verifyServerConfiguration
import com.gitlab.plugin.util.LINKS
import com.gitlab.plugin.util.TokenUtil
import com.intellij.ide.BrowserUtil
import com.intellij.openapi.options.BoundConfigurable
import com.intellij.openapi.options.ConfigurationException
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.dsl.builder.Cell
import com.intellij.ui.dsl.builder.bindSelected
import com.intellij.ui.dsl.builder.bindText
import com.intellij.ui.dsl.builder.panel
import java.util.*
import javax.swing.JLabel

class GitLabSettingsConfigurable : BoundConfigurable(GitLabBundle.message("settings.ui.group.name")) {

  private var tokenText = TokenUtil.getToken() ?: ""
  private val bundle: ResourceBundle = ResourceBundle.getBundle("messages.GitLabBundle")
  private val settings = DuoPersistentSettings.getInstance()

  private val urlErrorMessage = bundle.getString("settings.ui.error.url.invalid")
  private val tokenLabel = bundle.getString("settings.ui.gitlab.token")
  private val createTokenLabel = bundle.getString("settings.ui.gitlab.create-token")
  private val hostLabel = bundle.getString("settings.ui.gitlab.url")
  private val docsLabel = bundle.getString("settings.ui.gitlab.docs-link-label")
  private val telemetryLabel = bundle.getString("settings.ui.gitlab.enable-telemetry")
  private val verifySetupLabel = bundle.getString("settings.ui.gitlab.verify-setup")

  private var settingsPanel: DialogPanel? = null
  private var verifyLabel: Cell<JLabel>? = null

  override fun createPanel(): DialogPanel {
    var tokenValue: String = tokenText

    settingsPanel = panel {
      row {
        browserLink(docsLabel, LINKS.CODE_SUGGESTIONS_SETUP_DOCS_URL)
      }

      groupRowsRange("Connection") {
        row(hostLabel) {
          textField().bindText(settings::url)
            .addValidationRule(urlErrorMessage) { it.text.isBlank() }
        }
        row(tokenLabel) {
          passwordField()
            .bindText(::tokenText)
            .focused()
            .onChanged { component -> tokenValue = String(component.password) }
          button(createTokenLabel) { BrowserUtil.open("${settings.url}${LINKS.CREATE_TOKEN_PATH}") }
        }

        row {
          button(verifySetupLabel) { verifyServerConfiguration(settings.url, tokenValue, verifyLabel!!.component) }
          verifyLabel = label("")
        }
      }

      groupRowsRange("Advanced") {
        row {
          checkBox(telemetryLabel).bindSelected(settings::telemetryEnabled)
        }
      }

      groupRowsRange("Experimental") {
        row {
          checkBox(bundle.getString("settings.ui.gitlab.enable-suggestion-formatting"))
            .bindSelected(settings::suggestionFormattingEnabled)
        }
      }
    }

    return settingsPanel as DialogPanel
  }

  override fun apply() {
    val panel = settingsPanel ?: return
    val validationMessages = panel.validateAll()

    if (validationMessages.isEmpty()) {
      super.apply()

      TokenUtil.setToken(tokenText)
    } else {
      throw ConfigurationException(validationMessages.joinToString(separator = "\n") { it.message })
    }
  }
}
