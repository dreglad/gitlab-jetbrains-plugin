package com.gitlab.plugin.ui

import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.util.gitlabStatusEnabled
import com.gitlab.plugin.util.gitlabStatusError
import com.gitlab.plugin.util.gitlabStatusLoading

class IconStatusModifier : DuoClient.DuoClientRequestListener {
  override fun onRequestStart() = gitlabStatusLoading()

  override fun onRequestSuccess() = gitlabStatusEnabled()

  override fun onRequestError() = gitlabStatusError()
}
