package com.gitlab.plugin.ui

import com.gitlab.plugin.api.GitLabForbiddenException
import com.gitlab.plugin.api.GitLabOfflineException
import com.gitlab.plugin.api.GitLabProxyException
import com.gitlab.plugin.api.GitLabUnauthorizedException
import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.ExceptionHandler
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.services.ProjectContextService
import kotlinx.coroutines.runBlocking
import javax.swing.JLabel

fun verifyServerConfiguration(host: String, token: String, updatableLabel: JLabel) = runBlocking {
  updatableLabel.text = "Verifying..."

  val baseErrorMessage = "Oops! Something is missing on the configuration."
  var errorMessage = baseErrorMessage
  val client = ProjectContextService.instance.duoHttpClient.copy(
    host = host,
    tokenProvider = { token },
    shouldRetry = false,
    exceptionHandler = object : ExceptionHandler {
      override fun handleException(exception: Throwable) {
        errorMessage = when (exception) {
          is GitLabOfflineException -> "Server cannot be reached."
          is GitLabForbiddenException -> "Personal Access Token is insufficient."
          is GitLabUnauthorizedException ->
            "Personal Access Token is invalid or Code Suggestions is not available on your instance"

          is GitLabProxyException -> "Proxy is not setup correctly."
          else -> baseErrorMessage
        }
      }
    }
  )

  val verified = DuoApi(client, duoSettings = DuoPersistentSettings()).verify()

  if (verified) {
    updatableLabel.text = "Success! The server host and Personal Access Token are correctly setup"
  } else {
    updatableLabel.text = errorMessage
  }
}
