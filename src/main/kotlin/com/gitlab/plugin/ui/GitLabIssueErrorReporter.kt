package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.openapi.application.ApplicationInfo
import com.intellij.openapi.diagnostic.ErrorReportSubmitter
import com.intellij.openapi.diagnostic.IdeaLoggingEvent
import com.intellij.openapi.diagnostic.SubmittedReportInfo
import com.intellij.openapi.util.SystemInfo
import com.intellij.util.Consumer
import io.ktor.http.*
import java.awt.Component

class GitLabIssueErrorReporter : ErrorReportSubmitter() {
  override fun getReportActionText() = GitLabBundle.message("error-reporter.action-text")

  override fun submit(
    events: Array<out IdeaLoggingEvent>,
    additionalInfo: String?,
    parentComponent: Component,
    consumer: Consumer<in SubmittedReportInfo>
  ): Boolean {
    events.forEach { event ->
      val issueUrl = gitlabIssueUrl(event, additionalInfo.orEmpty())
      GitLabUtil.browseUrl(issueUrl)
    }

    consumer.consume(SubmittedReportInfo(SubmittedReportInfo.SubmissionStatus.NEW_ISSUE))

    return true
  }

  private fun gitlabIssueUrl(event: IdeaLoggingEvent, additionalInfo: String): String {
    val ideInfo = ApplicationInfo.getInstance().let { appInfo ->
      appInfo.fullApplicationName + "; Build: " + appInfo.build.asString()
    }

    val jdkInfo = with(System.getProperties()) {
      getProperty("java.version", "unknown") +
        "; VM: " + getProperty("java.vm.name", "unknown") +
        "; Vendor: " + getProperty("java.vendor", "unknown")
    }

    val osInfo = "${SystemInfo.getOsNameAndVersion()}; Arch: ${SystemInfo.OS_ARCH}"

    val description = """
      |## Debug information
      |
      |<details><summary>Stack trace</summary>
      |
      |```
      |${event.throwableText ?: "(no stack trace)"}
      |```
      |
      |</details>
      |
      |### Environment
      |
      |- Plugin version: ${GitLabBundle.plugin()?.version ?: "unknown"}
      |- IDE: $ideInfo
      |- JDK: $jdkInfo
      |- OS: $osInfo
      |
      |### Additional information
      |
      |$additionalInfo
    """.trimMargin().encodeURLParameter()

    return "https://gitlab.com/gitlab-org/editor-extensions/gitlab-jetbrains-plugin/-/issues/new" +
      "?issuable_template=Bug&issue[description]=$description"
  }
}
