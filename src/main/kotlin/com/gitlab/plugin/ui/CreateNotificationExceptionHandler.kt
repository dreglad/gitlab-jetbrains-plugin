package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.api.*
import com.gitlab.plugin.api.duo.ExceptionHandler

class CreateNotificationExceptionHandler(
  private val notificationManager: GitLabNotificationManager = GitLabNotificationManager(),
  private val settingsAction: NotificationAction = NotificationAction.settings(null)
) :
  ExceptionHandler {

  inner class ExceptionNotification<T : Throwable>(private val message: (ex: T) -> String) {
    private var shown: Boolean = false
    private val titleMessage = GitLabBundle.message("notification.title.gitlab-duo")

    fun show(exception: T, actions: List<NotificationAction> = listOf()) {
      if (shown) return

      val notification = Notification(titleMessage, message(exception), actions)

      notificationManager.sendNotification(notification, null)
      shown = true
    }

    fun reset() {
      shown = false
    }
  }

  private val offlineNotification =
    ExceptionNotification<GitLabOfflineException> { GitLabBundle.message("notification.exception.offline.message") }
  private val unauthorizedNotification =
    ExceptionNotification<GitLabUnauthorizedException> {
      GitLabBundle.message(
        "notification.exception.unauthorized.message"
      )
    }
  private val responseNotification =
    ExceptionNotification<GitLabResponseException> {
      GitLabBundle.message(
        "notification.exception.response.message",
        it.cachedResponseText
      )
    }
  private val proxyNotification =
    ExceptionNotification<GitLabProxyException> { GitLabBundle.message("notification.exception.proxy-failure.message") }
  private val forbiddenNotification =
    ExceptionNotification<GitLabForbiddenException> {
      GitLabBundle.message(
        "notification.exception.insufficient-scope.message"
      )
    }

  private val notifications = listOf(
    offlineNotification,
    unauthorizedNotification,
    responseNotification,
    forbiddenNotification,
    proxyNotification
  )

  override fun handleException(exception: Throwable) = when (exception) {
    is GitLabForbiddenException -> forbiddenNotification.show(exception, listOf(settingsAction))
    is GitLabUnauthorizedException -> unauthorizedNotification.show(exception, listOf(settingsAction))
    is GitLabOfflineException -> offlineNotification.show(exception)
    is GitLabResponseException -> responseNotification.show(exception)
    is GitLabProxyException -> proxyNotification.show(exception)
    else -> super.handleException(exception)
  }

  fun resetNotifications() = notifications.map { it.reset() }
}
