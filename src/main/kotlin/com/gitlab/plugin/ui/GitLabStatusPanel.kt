package com.gitlab.plugin.ui

import com.gitlab.plugin.GitLabSettingsChangeEvent
import com.gitlab.plugin.GitLabSettingsListener
import com.gitlab.plugin.services.DuoContextService
import com.gitlab.plugin.util.Status
import com.gitlab.plugin.util.gitlabStatusError
import com.gitlab.plugin.util.gitlabStatusRefresh
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.IconLoader
import com.intellij.openapi.util.NlsContexts
import com.intellij.openapi.wm.StatusBar
import com.intellij.openapi.wm.StatusBarWidget
import com.intellij.util.Consumer
import com.intellij.util.messages.MessageBusConnection
import java.awt.event.MouseEvent
import java.util.*
import javax.swing.Icon

class GitLabStatusPanel(project: Project) : StatusBarWidget, StatusBarWidget.IconPresentation {
  private val bundle: ResourceBundle = ResourceBundle.getBundle("messages.GitLabBundle")
  private var myStatusBar: StatusBar? = null
  private val myConnection: MessageBusConnection

  var status: Status = Status.Loading

  init {
    val duoContext = DuoContextService.instance

    status = if (duoContext.isDuoConfigured() && duoContext.isDuoEnabled()) {
      Status.Enabled
    } else {
      Status.Disabled
    }

    myConnection = project.messageBus.connect(this)
    registerListeners(myConnection)
  }

  override fun ID(): String = WIDGET_ID
  override fun getPresentation() = this
  override fun getTooltipText(): String? = getGitLabState().toolTip

  override fun getIcon(): Icon? = getGitLabState().icon

  override fun install(statusBar: StatusBar) {
    myStatusBar = statusBar
  }

  override fun getClickConsumer() = Consumer<MouseEvent> {
    toggleStatus()
  }

  override fun dispose() {
    myStatusBar = null
  }

  fun refresh() {
    myStatusBar?.updateWidget(ID())
  }

  private fun toggleStatus() {
    val duoContext = DuoContextService.instance
    val duoSettings = duoContext.duoSettings

    if (duoContext.isDuoConfigured()) {
      duoSettings.toggleEnabled()

      gitlabStatusRefresh()
    } else {
      val notification = Notification(
        "GitLab Duo",
        "You need to configure your GitLab credentials first.",
        listOf(NotificationAction.settings(null))
      )

      gitlabStatusError()
      GitLabNotificationManager().sendNotification(notification)
    }

    refresh()
  }

  private fun registerListeners(connection: MessageBusConnection) {
    connection.subscribe(
      GitLabSettingsListener.SETTINGS_CHANGED,
      object : GitLabSettingsListener {
        override fun codeStyleSettingsChanged(event: GitLabSettingsChangeEvent) {
          status = event.status

          refresh()
        }
      }
    )
  }

  private fun getGitLabState(): WidgetState {
    val iconPath: String
    val tooltipId: String

    when (status) {
      Status.Enabled -> {
        tooltipId = "status-icon.tooltip.enabled"
        iconPath = "/icons/gitlab-code-suggestions-enabled.svg"
      }

      Status.Disabled -> {
        tooltipId = "status-icon.tooltip.disabled"
        iconPath = "/icons/gitlab-code-suggestions-disabled.svg"
      }

      Status.Error -> {
        tooltipId = "status-icon.tooltip.error"
        iconPath = "/icons/gitlab-code-suggestions-error.svg"
      }

      Status.Loading -> {
        tooltipId = "status-icon.tooltip.loading"
        iconPath = "/icons/gitlab-code-suggestions-loading.svg"
      }
    }

    return WidgetState(bundle.getString(tooltipId), IconLoader.getIcon(iconPath, javaClass))
  }

  companion object {
    @JvmField
    val WIDGET_ID: String = GitLabStatusPanel::class.java.name
  }

  class WidgetState(
    val toolTip: @NlsContexts.Tooltip String?,
    val icon: Icon?
  )
}
