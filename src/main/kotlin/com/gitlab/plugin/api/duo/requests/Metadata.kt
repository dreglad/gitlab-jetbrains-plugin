package com.gitlab.plugin.api.duo.requests

class Metadata {
  data class Response(
    val version: String
  )
}
