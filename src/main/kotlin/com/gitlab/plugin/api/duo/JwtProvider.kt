package com.gitlab.plugin.api.duo

import com.gitlab.plugin.util.JwtSession

class JwtProvider : DuoClient.TokenProvider {
  override fun token(): String = JwtSession.getInstance().getJWT()
}
