package com.gitlab.plugin.api

import com.gitlab.plugin.util.GitLabUtil
import com.intellij.collaboration.util.resolveRelative
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.network.sockets.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import java.net.URI
import java.nio.channels.UnresolvedAddressException

private const val DEFAULT_SERVER_URL = "https://gitlab.com"

class GitLabApi(private val token: String, gitlabHost: String = DEFAULT_SERVER_URL) {
  private val baseUrl: URI

  init {
    baseUrl = URI(gitlabHost)
  }

  val restClient = HttpClient(proxiedEngine()) {
    expectSuccess = true

    install(UserAgent) {
      agent = GitLabUtil.userAgent
    }
    install(HttpRequestRetry) {
      retryOnServerErrors(maxRetries = 3)
      exponentialDelay()
    }
    install(Auth) {
      bearer {
        loadTokens {
          BearerTokens(token, "")
        }
      }
    }
    install(ContentNegotiation) {
      json(
        Json {
          isLenient = true
        }
      )
    }

    HttpResponseValidator {
      handleResponseExceptionWithRequest { cause: Throwable, request: HttpRequest ->
        when (cause) {
          is ClientRequestException -> {
            val exceptionResponse = cause.response
            val exceptionResponseText = exceptionResponse.bodyAsText()

            when (exceptionResponse.status) {
              HttpStatusCode.Unauthorized -> throw GitLabUnauthorizedException(exceptionResponse, exceptionResponseText)
              HttpStatusCode.Forbidden -> throw GitLabForbiddenException(exceptionResponse, exceptionResponseText)
              else -> throw GitLabResponseException(exceptionResponse, exceptionResponseText)
            }
          }

          is UnresolvedAddressException -> throw GitLabOfflineException(request, cause)
          is ConnectTimeoutException -> throw GitLabOfflineException(request, cause)
          is HttpRequestTimeoutException -> throw GitLabOfflineException(request, cause)
          else -> throw cause
        }
      }
    }
  }

  val apiUri: URI
    get() = baseUrl.resolveRelative("api/v4/")

  /**
   * Return whether is correctly configured to perform requests
   */
  fun isConfigured(): Boolean {
    return token.isNotEmpty()
  }

  /**
   * Generate code suggestions token
   *
   * @return CodeSuggestionsTokenDto - an object containing the access token and expiration metadata
   */
  @Throws(GitLabUnauthorizedException::class, GitLabOfflineException::class)
  suspend fun generateCodeSuggestionsToken(): CodeSuggestionsTokenDto? {
    val uri = apiUri.resolveRelative("code_suggestions/tokens").toString()

    val response: HttpResponse = restClient.post(uri) {
      contentType(ContentType.Application.Json)
    }

    return response.body<CodeSuggestionsTokenDto>()
  }
}
