package com.gitlab.plugin.services

import com.gitlab.plugin.api.duo.DuoApi
import kotlinx.coroutines.runBlocking
import java.lang.module.ModuleDescriptor.Version
import kotlin.time.Duration.Companion.hours
import kotlin.time.Duration.Companion.milliseconds

class GitLabServerService(
  private val api: DuoApi
) {
  private var server =
    Server(
      version = Version.parse("0.0.0"),
      lastUpdated = 0
    )

  fun get(): Server {
    if (isExpired()) runBlocking { update() }

    return server
  }

  private fun isExpired() = (System.currentTimeMillis() - server.lastUpdated).milliseconds > 2.hours

  private suspend fun update() {
    val metadataResponse = api.metadata() ?: return

    server = Server(
      version = Version.parse(metadataResponse.version),
      lastUpdated = System.currentTimeMillis()
    )
  }

  data class Server(
    val version: Version,
    val lastUpdated: Long
  ) {
    fun after162() = this.version > Version.parse("16.2")
  }
}
