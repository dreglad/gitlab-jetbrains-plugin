package com.gitlab.plugin.codesuggestions

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.requests.Completion
import com.gitlab.plugin.codesuggestions.rules.CurrentlyDisplayingCompletion
import com.gitlab.plugin.codesuggestions.rules.CursorOutOfBounds
import com.gitlab.plugin.codesuggestions.rules.DisallowedCharactersPastCursor
import com.gitlab.plugin.codesuggestions.rules.UnsupportedFileExtension
import com.gitlab.plugin.services.ProjectContextService
import com.gitlab.plugin.ui.CompletionStrategy
import com.gitlab.plugin.util.removePrefix
import com.intellij.codeInsight.inline.completion.*
import com.intellij.codeInsight.inline.completion.elements.InlineCompletionGrayTextElement
import com.intellij.openapi.util.TextRange
import kotlinx.coroutines.CancellationException
import org.jetbrains.annotations.ApiStatus
import kotlin.time.Duration
import kotlin.time.Duration.Companion.seconds

val BLOCKED_CHARS = setOf("[]", "{}", "()")

@ApiStatus.Experimental
internal class SuggestionsProvider : DebouncedInlineCompletionProvider() {
  override val delay: Duration = 1.seconds
  override val id = InlineCompletionProviderID(this::class.java.name)

  private val api: DuoApi by lazy { ProjectContextService.instance.duoApi }
  private val completionStrategy: CompletionStrategy by lazy { ProjectContextService.instance.completionStrategy }
  private val skipSuggestionRules = listOf(
    UnsupportedFileExtension(),
    CursorOutOfBounds(),
    CurrentlyDisplayingCompletion(),
    DisallowedCharactersPastCursor()
  )

  override suspend fun getSuggestionDebounced(request: InlineCompletionRequest): InlineCompletionSuggestion {
    if (skipSuggestionRules.any { it.shouldSkipSuggestion(request) }) return InlineCompletionSuggestion.empty()

    val payload = preparePayload(request)

    val response = completionStrategy.generateCompletions(payload) ?: return InlineCompletionSuggestion.empty()

    if (response.choices.isEmpty()) return InlineCompletionSuggestion.empty()

    return InlineCompletionSuggestion.withFlow {
      emit(InlineCompletionGrayTextElement(response.choices[0].text))
    }
  }

  override fun shouldBeForced(request: InlineCompletionRequest): Boolean = false

  override suspend fun getSuggestion(request: InlineCompletionRequest): InlineCompletionSuggestion {
    return try {
      super.getSuggestion(request)
    } catch (e: CancellationException) {
      // Workaround to catch JobCancellationException thrown due to job cancel due to debounce.
      InlineCompletionSuggestion.empty()
    }
  }

  override fun isEnabled(event: InlineCompletionEvent): Boolean = !withinBoundingChar(event) && api.isApiEnabled()

  private fun preparePayload(request: InlineCompletionRequest): Completion.Payload {
    val document = request.document
    val content = document.charsSequence
    val currentPosition: Int = request.endOffset

    val aboveCursor = content.take(currentPosition).toString()
    val belowCursor = content.drop(currentPosition).toString()

    val fileName = removePrefix(request.file.virtualFile.path, request.editor.project?.basePath)

    return Completion.Payload(
      currentFile = Completion.Payload.CodeSuggestionRequestFile(
        fileName ?: request.file.name,
        aboveCursor,
        belowCursor
      ),
      telemetry = emptyList()
    )
  }

  private fun withinBoundingChar(event: InlineCompletionEvent): Boolean {
    val request = event.toRequest() ?: return false

    val currentPosition = request.endOffset
    return currentPosition > 1 &&
      BLOCKED_CHARS.contains(request.document.getText(TextRange(currentPosition - 2, currentPosition)))
  }
}
