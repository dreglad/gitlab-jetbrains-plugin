package com.gitlab.plugin.codesuggestions.telemetry

import java.util.*

class Telemetry(
  private val destinations: List<Destination> = listOf(LogDestination()),
  private val generateUUID: () -> String = { UUID.randomUUID().toString() },
  private val isEnabled: () -> Boolean
) {
  interface Destination {
    fun event(event: Event)
  }

  companion object {
    val NOOP by lazy { Telemetry(destinations = emptyList(), isEnabled = { false }) }
  }

  var currentContext: Event.Context = newContext()
    private set

  fun requested(context: Event.Context) {
    currentContext = context
    broadcast(Event.Type.REQUESTED, context)
  }

  fun loaded(context: Event.Context) {
    currentContext = context
    broadcast(Event.Type.LOADED, context)
  }

  fun accepted(context: Event.Context) = broadcast(Event.Type.ACCEPTED, context)
  fun error(context: Event.Context) = broadcast(Event.Type.ERROR, context)
  fun cancelled(context: Event.Context) = broadcast(Event.Type.CANCELLED, context)
  fun notProvided(context: Event.Context) = broadcast(Event.Type.NOT_PROVIDED, context)
  fun shown(context: Event.Context) = broadcast(Event.Type.SHOWN, context)
  fun rejected(context: Event.Context) = broadcast(Event.Type.REJECTED, context)

  fun newContext() = Event.Context(uuid = generateUUID())

  private fun broadcast(type: Event.Type, context: Event.Context) {
    if (!isEnabled()) return

    val event = Event(type, context)

    destinations.forEach {
      it.event(event)
    }
  }
}
