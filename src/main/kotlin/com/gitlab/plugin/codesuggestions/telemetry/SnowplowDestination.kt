package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.api.ProxyManager
import com.gitlab.plugin.api.asProxyInfo
import com.gitlab.plugin.util.GitLabUtil
import com.intellij.util.net.HttpConfigurable
import com.snowplowanalytics.snowplow.tracker.Snowplow
import com.snowplowanalytics.snowplow.tracker.Tracker
import com.snowplowanalytics.snowplow.tracker.configuration.EmitterConfiguration
import com.snowplowanalytics.snowplow.tracker.configuration.NetworkConfiguration
import com.snowplowanalytics.snowplow.tracker.configuration.SubjectConfiguration
import com.snowplowanalytics.snowplow.tracker.configuration.TrackerConfiguration
import com.snowplowanalytics.snowplow.tracker.events.Structured
import com.snowplowanalytics.snowplow.tracker.http.OkHttpClientAdapter
import okhttp3.OkHttpClient

class SnowplowDestination(private val tracker: Tracker) : Telemetry.Destination {
  companion object {
    const val EVENT_CATEGORY = "code_suggestions"
    const val EMITTER_BATCH_SIZE = 50
    const val COLLECTOR_URL = "https://snowplow.trx.gitlab.net"

    private const val NAMESPACE = "gl"
    private const val APP_ID = "gitlab_ide_extension"
    private val IDE_EXTENSION_VERSION_CONTEXT by lazy { IdeExtensionVersionContext.build() }

    fun createTracker(collectorUrl: String, batchSize: Int): Tracker {
      val client = OkHttpClient.Builder().buildWithProxySupport()
      val trackerConfiguration = TrackerConfiguration(NAMESPACE, APP_ID)
      val networkConfiguration = NetworkConfiguration(OkHttpClientAdapter(collectorUrl, client))
      val emitterConfiguration = EmitterConfiguration().batchSize(batchSize)
      val subjectConfiguration = SubjectConfiguration().useragent(GitLabUtil.userAgent)

      return Snowplow.createTracker(
        trackerConfiguration,
        networkConfiguration,
        emitterConfiguration,
        subjectConfiguration
      )
    }
  }

  override fun event(event: Event) {
    with(Structured.builder()) {
      category(EVENT_CATEGORY)
      action(event.action)
      label(event.context.uuid)
      customContext(
        listOf(
          IDE_EXTENSION_VERSION_CONTEXT,
          CodeSuggestionsContext.build(event.context)
        )
      )
      build()
    }.let {
      tracker.track(it)
    }
  }
}

private fun OkHttpClient.Builder.buildWithProxySupport(): OkHttpClient {
  followRedirects(true)

  val proxyInfo = HttpConfigurable.getInstance().asProxyInfo()
  if (proxyInfo != null) {
    val proxyManager = ProxyManager(proxyInfo) { HttpConfigurable.getInstance().isHttpProxyEnabledForUrl(it) }

    proxySelector(proxyManager)
    proxyAuthenticator(proxyManager)
  }

  return build()
}
