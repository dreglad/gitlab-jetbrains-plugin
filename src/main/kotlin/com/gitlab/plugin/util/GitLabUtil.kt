package com.gitlab.plugin.util

import com.gitlab.plugin.GitLabBundle
import com.gitlab.plugin.GitLabSettingsChangeEvent
import com.gitlab.plugin.GitLabSettingsListener
import com.gitlab.plugin.services.DuoContextService
import com.intellij.collaboration.api.httpclient.HttpClientUtil
import com.intellij.ide.BrowserUtil
import com.intellij.openapi.application.ApplicationManager

object GitLabUtil {
  const val SERVICE_NAME = "com.gitlab.plugin"
  const val GITLAB_DEFAULT_URL = "https://gitlab.com"

  val userAgent by lazy {
    HttpClientUtil.getUserAgentValue("gitlab-jetbrains-plugin/${GitLabBundle.plugin()?.version ?: "DEV"}")
  }

  fun browseUrl(url: String) = BrowserUtil.browse(url)
}

enum class Status {
  Enabled,
  Disabled,
  Error,
  Loading,
}

fun gitlabStatusChangedNotify(status: Status) {
  ApplicationManager.getApplication().messageBus.syncPublisher(GitLabSettingsListener.SETTINGS_CHANGED)
    .codeStyleSettingsChanged(GitLabSettingsChangeEvent(status))
}

fun gitlabStatusError() {
  gitlabStatusChangedNotify(Status.Error)
}

fun gitlabStatusLoading() {
  gitlabStatusChangedNotify(Status.Loading)
}

fun gitlabStatusEnabled() {
  gitlabStatusChangedNotify(Status.Enabled)
}

fun gitlabStatusDisabled() {
  gitlabStatusChangedNotify(Status.Disabled)
}

fun gitlabStatusRefresh() {
  val duoContext = DuoContextService.instance

  if (duoContext.isDuoConfigured() && duoContext.isDuoEnabled()) {
    gitlabStatusEnabled()
  } else {
    gitlabStatusDisabled()
  }
}

object LINKS {
  const val CODE_SUGGESTIONS_SETUP_DOCS_URL =
    "https://docs.gitlab.com/ee/user/project/repository/code_suggestions.html#enable-code-suggestions-on-gitlab-saas"

  const val CREATE_TOKEN_PATH =
    "/-/profile/personal_access_tokens?name=GitLab%20Duo%20For%20JetBrains&scopes=ai_features"
}
