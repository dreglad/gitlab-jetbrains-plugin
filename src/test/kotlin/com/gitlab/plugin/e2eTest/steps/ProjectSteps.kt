package com.gitlab.plugin.e2eTest.steps

import com.gitlab.plugin.e2eTest.pages.dialog
import com.gitlab.plugin.e2eTest.pages.idea
import com.gitlab.plugin.e2eTest.pages.welcomeFrame
import com.intellij.remoterobot.RemoteRobot
import com.intellij.remoterobot.stepsProcessing.step
import com.intellij.remoterobot.utils.waitFor
import java.time.Duration

class ProjectSteps(private val remoteRobot: RemoteRobot) {

  fun createNewProject() = with(remoteRobot) {
    step(
      "Create New Project",
      Runnable {
        welcomeFrame {
          createNewProjectLink.click()
          dialog("New Project") {
            findText("Java").click()
            checkBox("Add sample code").select()
            button("Create").click()
          }
        }

        // wait for IntelliJ indexing to complete
        idea {
          waitFor(Duration.ofMinutes(5)) { isDumbMode().not() }
        }
      }
    )
  }

  fun closeProject() = with(remoteRobot) {
    step(
      "Close Project",
      Runnable {
        idea {
          menuBar.select("File", "Close Project")
        }
      }
    )
  }
}
