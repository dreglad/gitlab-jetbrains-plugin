package com.gitlab.plugin.codesuggestions.rules

import com.intellij.codeInsight.inline.completion.InlineCompletionRequest
import com.intellij.openapi.diagnostic.Logger
import io.kotest.assertions.withClue
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.datatest.withData
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import io.mockk.*

class UnsupportedFileExtensionTest : DescribeSpec({
  val logger: Logger = mockk()
  val request: InlineCompletionRequest = mockk()
  val unsupportedFileExtension = UnsupportedFileExtension(logger)

  describe("returns false for supported extensions") {
    withData(UnsupportedFileExtension.SUPPORTED_EXTENSIONS) { extension ->
      every { request.file.fileType.defaultExtension } returns extension

      withClue("returns false for $extension extension") {
        unsupportedFileExtension.shouldSkipSuggestion(request) shouldBe false
      }

      withClue("does not log a message") {
        verify(exactly = 0) { logger.info(any<String>()) }
      }

      clearMocks(request, logger)
    }
  }

  describe("returns true for unsupported extensions") {
    withData(listOf("yaml", "xml", "txt", "json")) { extension ->
      every { request.file.fileType.defaultExtension } returns extension
      val logMessageSlot = slot<String>()
      every { logger.info(capture(logMessageSlot)) } just runs

      withClue("returns true for $extension extension") {
        unsupportedFileExtension.shouldSkipSuggestion(request) shouldBe true
      }

      withClue("logs a message") {
        verify(exactly = 1) { logger.info(any<String>()) }
        logMessageSlot.captured shouldContain "file extension \"$extension\" is not supported"
      }

      clearMocks(request, logger)
    }
  }
})
