package com.gitlab.plugin.codesuggestions.telemetry

import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.util.GitLabUtil
import io.kotest.core.spec.style.DescribeSpec
import io.kotest.matchers.maps.shouldContainAll
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.unmockkAll

class CodeSuggestionsContextTest : DescribeSpec({
  mockkObject(DuoPersistentSettings, GitLabUtil)

  val eventContext = Event.Context(
    uuid = "uuid",
    language = "js",
    modelEngine = "vertex-ai",
    modelName = "code-gecko@latest",
    prefixLength = 17,
    suffixLength = 0,
    apiStatusCode = 200
  )

  beforeEach {
    every { GitLabUtil.userAgent } returns "gitlab-jetbrains-plugin/0.4.1"
    every { DuoPersistentSettings.getInstance().gitlabRealm() } returns "saas"
  }

  afterEach { clearAllMocks() }
  afterSpec { unmockkAll() }

  it("returns a self-describing JSON with the correct keys and values") {
    CodeSuggestionsContext.build(eventContext).let {
      it.map shouldContainAll mapOf(
        "schema" to CodeSuggestionsContext.SCHEMA,
        "data" to mapOf(
          "gitlab_realm" to "saas",
          "language" to "js",
          "model_engine" to "vertex-ai",
          "model_name" to "code-gecko@latest",
          "prefix_length" to 17,
          "suffix_length" to 0,
          "user_agent" to "gitlab-jetbrains-plugin/0.4.1",
          "api_status_code" to 200
        )
      )
    }
  }
})
