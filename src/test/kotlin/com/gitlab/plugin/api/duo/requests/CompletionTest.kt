package com.gitlab.plugin.api.duo.requests

import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

@Suppress("ClassName")
class CompletionTest {
  @Nested
  inner class Response {
    @Nested
    inner class hasSuggestions {
      private val model = Completion.Response.Model()

      @Nested
      inner class `when choices is an empty list` {
        @Test
        fun `returns false`() {
          Completion.Response(emptyList(), model).hasSuggestions() shouldBe false
        }
      }

      @Nested
      inner class `when all choices are empty strings` {
        @Test
        fun `returns false`() {
          val choices = listOf(Completion.Response.Choice(""), Completion.Response.Choice(" "))

          Completion.Response(choices, model).hasSuggestions() shouldBe false
        }
      }

      @Nested
      inner class `when suggestions are provided` {
        @Test
        fun `returns true`() {
          val choices = listOf(Completion.Response.Choice("A code suggestion"))

          Completion.Response(choices, model).hasSuggestions() shouldBe true
        }
      }
    }
  }
}
