package com.gitlab.plugin.api

import com.gitlab.plugin.api.duo.DuoApi
import com.gitlab.plugin.api.duo.DuoClient
import com.gitlab.plugin.api.duo.ExceptionHandler
import com.gitlab.plugin.authentication.DuoPersistentSettings
import com.gitlab.plugin.codesuggestions.telemetry.Telemetry
import io.ktor.client.engine.*
import io.ktor.client.engine.mock.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.utils.io.*
import kotlinx.coroutines.runBlocking

fun captureRequest(block: suspend (httpEngine: HttpClientEngine) -> HttpResponse?):
  Pair<HttpRequestData, HttpResponse?> = runBlocking {
  var data: HttpRequestData? = null

  val mockEngine = MockEngine { request ->
    data = request
    respond(content = ByteReadChannel(""))
  }

  val httpResponse = block.invoke(mockEngine)

  val requestData = data ?: throw RuntimeException("Request data not found")

  return@runBlocking Pair(requestData, httpResponse)
}

fun buildMockEngine(
  statusCode: HttpStatusCode = HttpStatusCode.OK,
  content: String = """{"some_property": "abc"}""",
  block: (HttpRequestData) -> Unit = {}
): HttpClientEngine = MockEngine { request ->
  block.invoke(request)

  respond(
    content = ByteReadChannel(content),
    status = statusCode,
    headers = headersOf(HttpHeaders.ContentType, "application/json")
  )
}

fun buildClient(
  tokenProvider: DuoClient.TokenProvider = DuoClient.TokenProvider { "some_pat" },
  jwtProvider: DuoClient.TokenProvider = DuoClient.TokenProvider { "some_jwt" },
  exceptionHandler: ExceptionHandler = object : ExceptionHandler {},
  httpClientEngine: HttpClientEngine = buildMockEngine(),
  userAgent: String = "gitlabUserAgent",
  onStatusChanged: DuoClient.DuoClientRequestListener = object : DuoClient.DuoClientRequestListener {},
  shouldRetry: Boolean = false
) = DuoClient(
  tokenProvider,
  jwtProvider,
  exceptionHandler,
  userAgent,
  shouldRetry,
  httpClientEngine,
  "https://gitlab.com",
  onStatusChanged
)

data class TestRequest<T>(val data: HttpRequestData, val response: T)

suspend fun <T> makeTestRequest(content: String, block: suspend (DuoApi) -> T): TestRequest<T> {
  var data: HttpRequestData? = null
  val mockEngine =
    buildMockEngine(content = content) { request -> data = request }

  val api = buildApi(
    buildClient(httpClientEngine = mockEngine, tokenProvider = { "pat" }, jwtProvider = { "jwt" })
  )

  val response = block.invoke(api)
  val requestData = data ?: throw RuntimeException("Request data not found")

  return TestRequest(requestData, response)
}

fun buildApi(client: DuoClient = buildClient(), telemetry: Telemetry = Telemetry.NOOP, duoSettings: DuoPersistentSettings = DuoPersistentSettings()) =
  DuoApi(client, telemetry, duoSettings)

fun mockClient(content: String = """{"choices":[],"model":{}}"""): DuoClient {
  val mockEngine = buildMockEngine(content = content)
  return buildClient(
    httpClientEngine = mockEngine,
    tokenProvider = { "pat" },
    jwtProvider = { "jwt" }
  )
}
