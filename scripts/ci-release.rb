#!/usr/bin/env ruby
# frozen_string_literal: true

$LOAD_PATH.unshift(__dir__) unless $LOAD_PATH.include?(__dir__)
require 'gitlab-utils'

version = find_current_version

puts "Preparing to publish: #{PLUGIN_NAME}"
puts "Version: #{version}"
puts "Artifacts:"

local_artifacts.each do |artifact|
  puts " - #{artifact}"
end

puts 'Changes:'
changelog = fetch_changes
changelog.each { |c| puts c }

puts "--------"
puts "Publishing Artifacts:"

local_artifacts.each do |artifact|
  puts " - #{artifact}:"

  package_repo = package(version)
  if package_repo && package_artifact_exist?(artifact, package_repo.id)
    puts "Skipping! (already published)"
  else
    publish_artifact!(artifact, version)
  end

  puts
end

print "Checking for existing release... "
release = find_release(version)
print_release = -> (data) { puts "- #{data.name} - #{data._links.self}" }

if release
  puts "Found:"
  print_release.(release)
  puts
  puts "INFO: If you want to overwrite or replace an existing release, delete it first!"
  exit 1
else
  puts "Not found! - Creating one:"

  published_artifacts = find_published_artifacts(local_artifacts, package(version))
  release = create_release!(version, published_artifacts, changelog: changelog)
  print_release.(release)
end
